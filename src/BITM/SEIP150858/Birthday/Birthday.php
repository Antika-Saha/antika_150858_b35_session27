<?php

namespace App\birthday;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Birthday extends DB
{
    public $id;

    public $name;

    public $birthday_date;



    public function __construct(){

        parent::__construct();

    }

    public function setData($postVariableData=NULL){
        if(array_key_exists('id',$postVariableData)){
            $this->id=$postVariableData['id'];
        }

        if(array_key_exists('birth_date',$postVariableData)){
            $this->birthday_date=$postVariableData['birth_date'];
        }

        if(array_key_exists('name',$postVariableData)){
            $this->name=$postVariableData['name'];
        }

    }


    public function store(){


        $arrData = array($this->birthday_date,$this->name);
        $sql = "Insert INTO birthday(birth_date,name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("success!!!! Data has been inserted successfully.... :) ");
        else
            Message::setMessage("Faild!! Data has not been inserted successfully.... :( ");

        Utility::redirect('creat.php');

    }//end of store


    public function index()
    {

    }

}


