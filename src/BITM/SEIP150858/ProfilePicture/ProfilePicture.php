<?php

namespace App\ProfilePicture;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
class ProfilePicture extends DB
{

    public $id = "";

    public $name = "users";

    public $profile_name = "";

    public $image_name = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($postVariableData=NULL){
        if(array_key_exists('id',$postVariableData)){
            $this->id=$postVariableData['id'];
        }

        if(array_key_exists('name',$postVariableData)){
            $this->name=$postVariableData['name'];
        }

        if(array_key_exists('profile_name',$postVariableData)){
            $this->profile_name=$postVariableData['profile_name'];
        }

        if(array_key_exists('image_name',$postVariableData)){
            $this->image_name=$postVariableData['image_name'];
        }

    }

    public function store(){


        $arrData = array($this->name,$this->profile_name,$this->image_name);
        $sql = "Insert INTO profile_picture(name,profile_name,image_name) VALUES (?,?,?)";

        $STH = $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("success!!!! Data has been inserted successfully.... :) ");
        else
            Message::setMessage("Faild!! Data has not been inserted successfully.... :( ");

        Utility::redirect('creat.php');

    }//end of store



    public function index()
    {

    }


}// end of BookTitle class